﻿using System.Collections.Generic;
using UnityEngine;

public static class ExtensionMethods
{

    public static Vector2 Rotate(this Vector2 v, float degrees)
    {
        float sin = Mathf.Sin(degrees * Mathf.Deg2Rad);
        float cos = Mathf.Cos(degrees * Mathf.Deg2Rad);

        float tx = v.x;
        float ty = v.y;
        v.x = (cos * tx) - (sin * ty);
        v.y = (sin * tx) + (cos * ty);
        return v;
    }

    public static Dir ToDir(this float angle)
    {
        angle = Mathf.Round((angle + 360f) / 90f % 4);
        switch (angle)
        {
            case 0: return Dir.right;
            case 1: return Dir.up;
            case 2: return Dir.left;
            case 3: return Dir.down;
        }
        return Dir.right;

    }
    public static float ToAngle(this Dir dir)
    {
        switch (dir)
        {
            case Dir.right: return 0;
            case Dir.up: return 90;
            case Dir.left: return 180;
            case Dir.down: return 270;
        }
        return 0;
    }
    public static Vector2 ToVec(this Dir dir)
    {
        switch (dir)
        {
            case Dir.right: return Vector2.right;
            case Dir.up: return Vector2.up;
            case Dir.left: return Vector2.left;
            case Dir.down: return Vector2.down;
        }
        return Vector2.zero;
    }

    public static Dir Flip(this Dir dir)
    {
        switch (dir)
        {
            case Dir.right: return Dir.left;
            case Dir.up: return Dir.down;
            case Dir.left: return Dir.right;
            case Dir.down: return Dir.up;
        }
        return Dir.left;
    }

    public static Vector3 Round(this Vector3 vector3)
    {
        return new Vector3(Mathf.Round(vector3.x), Mathf.Round(vector3.y), Mathf.Round(vector3.z));
    }

    public static bool TileAtPos(Vector2 pos)
    {
        pos = ((Vector3)pos).Round();
        List<Collider2D> list = new List<Collider2D>(Physics2D.OverlapAreaAll(pos + new Vector2(-0.1f, -0.1f), pos + new Vector2(0.1f, 0.1f)));
        list.AddRange(Physics2D.OverlapPointAll(pos));
        foreach (Collider2D col in list)
        {
            if (col.tag == "Ground" || col.tag == "Walls") { return true; }
        }
        return false;
    }
}