﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class GameManager : Singleton<GameManager>
{

    public int MinionLimitGoal = 10;
    [HideInInspector]
    public int Points = 0;

    private AudioSource audio;
    private void Start()
    {
        audio = GetComponent<AudioSource>();
        if (!audio.isPlaying)
        {
            audio.Play();
        }
    }


    private void Update()
    {
        if (SpawnManager.Instance != null)
        {
            if (SpawnManager.Instance.spawLimit == 0)
            {
                if (SceneManager.GetActiveScene().name == "LevelOne" && Points == 10)
                {
                    SceneManager.LoadScene("LevelTwo");
                    SpawnManager.Instance.ActivateSpaw();
                }
                if (SceneManager.GetActiveScene().name == "LevelTwo" && Points == 20)
                {
                    Application.Quit();
                }

            }
        }


    }

    public void MinionGoal()
    {
        Points += 10;
    }


}
