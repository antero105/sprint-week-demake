﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public interface IInput
{
    InputComponent input { get; set; }
}

public interface IOutput
{
    OutputComponent output { get; set; }

}

[Serializable]
public class OutputComponent
{
    //Variables as an output
    [SerializeField] public List<MonoBehaviour> outputs; //sends input to all outputs;

    protected bool _on;
    public bool on
    {
        get { return _on; }
        set
        {
            foreach (MonoBehaviour output in outputs)
            {
                if (output is IInput)
                {
                    if (!((IInput)output).input.currentInputs.Contains(this))
                    {
                        if (value) { ((IInput)output).input.currentInputs.Add(this); }
                    }
                    else
                    {
                        if (!value) { ((IInput)output).input.currentInputs.Remove(this); }
                    }
                ((IInput)output).input.checkActivated();
                }
            }
            _on = value;
        }
    }

}

[Serializable]
public class InputComponent
{

    //Variables as an input


    public bool defaultsOn = false;
    private bool prevOn;
    public bool on { get { return defaultsOn ^ currentInputs.Count >= requiredInputs; } }
    [HideInInspector] public List<OutputComponent> currentInputs; //list of outputters sending input;
    public int requiredInputs = 1;

    [SerializeField] public Action onActivate;
    [SerializeField] public Action onDeactivate;

    public void checkActivated()
    {
        if (on != prevOn)
        {
            if (on) { OnActivate(); } else { OnDeactivate(); }
        }
        prevOn = on;
    }
    //Used for inputs only
    protected void OnActivate()
    {
        if (onActivate != null)
        {
            onActivate.Invoke();
        }
    }

    protected void OnDeactivate()
    {
        if (onActivate != null)
        {
            onDeactivate.Invoke();
        }
    }

}