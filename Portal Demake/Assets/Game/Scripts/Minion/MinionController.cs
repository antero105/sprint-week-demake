﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

public class MinionController : MonoBehaviour
{

    public struct PreservedMomentum{
        public float duration;
        public Vector3 momentum;
    }
    PreservedMomentum preservedMomentum;

    public float speed = 5.0f;
    public enum DIRECTION
    {
        LEFT,
        RIGHT,
    }
    [HideInInspector]
    public UnityEvent CollisionEvent;

    [HideInInspector]
    public GameObject target;

    public Vector2 velocity { get { return GetComponent<Rigidbody2D>().velocity; } set { GetComponent<Rigidbody2D>().velocity = value; } }

    void Start()
    {
        target = GameObject.FindWithTag("Target");
    }

    // Update is called once per frame
    void Update()
    {
      
        if (preservedMomentum.duration >= 0) {
            preservedMomentum.duration -= Time.deltaTime;
            GetComponent<Rigidbody2D>().velocity = preservedMomentum.momentum;
        }
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
    
        if (collision.gameObject.tag == "Walls")
        {
            CollisionEvent.Invoke();
        }

        if (collision.gameObject.tag == "Target")
        {
            Destroy(gameObject);
            GameManager.Instance.MinionGoal();
        }


    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<Portal>().Teleport(gameObject, out float rotation, out Dir dir))
        {
            transform.Translate(Vector2.right.Rotate(dir.ToAngle()) * 2f, Space.World);

            preservedMomentum.duration = 0.25f;
            preservedMomentum.momentum = GetComponent<Rigidbody2D>().velocity.Rotate(rotation);

        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.GetComponent<Portal>() != null)
        {
            collision.GetComponent<Portal>().teleportedObjects.Remove(gameObject);
        }
    }
}
    
