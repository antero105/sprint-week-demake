﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Portal : MonoBehaviour
{

   [SerializeField] public List<GameObject> teleportedObjects;

    public Portal dest
    {
        get {
            if (PortalManager.Instance.bluePortal == this) { return PortalManager.Instance.redPortal; }
            return PortalManager.Instance.bluePortal;
        }
    }
    public bool dead
    {
        get
        {
            if (PortalManager.Instance.bluePortal == this || PortalManager.Instance.redPortal == this) { return false; }
            return true;
        }
    }

    public float rotation
    {
        get { return transform.rotation.eulerAngles.z; }
        set { transform.rotation = Quaternion.Euler(0, 0, value); }
    }

    public Dir dir
    {
        get { return rotation.ToDir(); }
        set { transform.rotation = Quaternion.Euler(0, 0, dir.ToAngle()); }
    }

    public Vector2 offset
    {
        get
        {
            return  dest.dir.ToVec()*0.6f;
        }

    }

    int width = 1;

    // Start is called before the first frame update
    void Start()
    {
        attemptExpand();
    }

    // Update is called once per frame
    void Update()
    {
        //Delete redundant portals
        if (dead) { Destroy(gameObject); }
    }

    //triples portal width
    void attemptExpand() {

        Vector2 initialPos =transform.position;
        Vector2 sideDir = dir.ToVec().Rotate(90)*(width/2f+0.4f);
        bool expandLeft = false;
        bool expandRight = false;
        if(ExtensionMethods.TileAtPos(initialPos+sideDir) &!ExtensionMethods.TileAtPos(initialPos + sideDir + dir.ToVec())){
            expandLeft = true;
        }
        if (ExtensionMethods.TileAtPos(initialPos - sideDir) & !ExtensionMethods.TileAtPos(initialPos - sideDir + dir.ToVec())){
            expandRight = true;
        }
        if (expandLeft && expandRight) { width = 3; }
        else if (expandLeft) { width = 2; transform.Translate(Vector2.up * 0.5f); }
        else if (expandRight) { width = 2; transform.Translate(Vector2.down * 0.5f); }
        transform.localScale = new Vector2(1f, width);
    }

    //TODO
    public bool Teleport(GameObject o)
    {
        return (Teleport(o, out float rotate, out Dir newDir));
    }
    public bool Teleport(GameObject o, out float rotate)
    {
        rotate = 0;
        if (Teleport(o, out  rotate, out Dir newDir)){
            return true;
        }
        return false;

    }
    public bool Teleport(GameObject o, out Dir newDir)
    {
        newDir = Dir.up;
        if (Teleport(o, out float rotate, out newDir)){
            return true;
        }
        return false;

    }
    
        public bool Teleport(GameObject o, out float rotate, out Dir newDir) {
        if (dest == null || teleportedObjects.Contains(o)) { rotate = 0; newDir = Dir.up;  return false; }
        rotate = transform.rotation.eulerAngles.z - dest.transform.rotation.eulerAngles.z + 180f;

        o.transform.localPosition = new Vector3(dest.transform.localPosition.x, dest.transform.localPosition.y,o.transform.localPosition.z)+(Vector3)offset;
        dest.teleportedObjects.Add(o);
        newDir = dest.dir;
        return true;
        //returns rotateAmount so object can handle its own rotation. i.e, falling objects need to conserve momentum.

    }


}
