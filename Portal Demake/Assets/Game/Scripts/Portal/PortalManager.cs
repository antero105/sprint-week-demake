﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PortalManager : Singleton<PortalManager>
{
    [SerializeField] public Portal redPortal;
    [SerializeField] public Portal bluePortal;

   public bool portalType;

    [SerializeField] public Portal redPortalPrefab;
    [SerializeField] public Portal bluePortalPrefab;

    [SerializeField] public float minDragDistance = 0.25f;

    Vector3 startDrag;
    private void Update()
    {
        //For debugging (until we do portal type UI)
        if (Input.GetKeyDown(KeyCode.Space)) { portalType = !portalType; }


        if (Input.GetMouseButtonUp(0))
        {
            Vector3 endDrag = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            endDrag.z = 0;

            if (portalType)
            {
                //TODO:Portal rotation
                redPortal = AttemptCreatePortal(redPortalPrefab, endDrag) ?? redPortal;
            }
            else
            {
                bluePortal = AttemptCreatePortal(bluePortalPrefab, endDrag) ?? bluePortal;
            }
        }
    }

    private Portal AttemptCreatePortal(Portal portal, Vector3 pos)
    {

        Vector2 roundPos = pos.Round();
        
        //create list of dirs to check. If the mouse was dragged, there is only one dir to check. On a tap, check all four directions.
        Dir[] dirs = new Dir[] { Dir.up, Dir.down, Dir.left, Dir.right };

        //Check if pos is an empty space. If so; it looks for a wall to place it on. If not, it checks if there's a direction that's open.

        Dir? bestDir = null;
        float shortestDist = 999;

        if (ExtensionMethods.TileAtPos(pos))
        {
            foreach (Dir checkdir in dirs)
            {
                if (!ExtensionMethods.TileAtPos(roundPos + checkdir.ToVec()))
                {
                    float dist = Vector2.Distance(pos, roundPos + checkdir.ToVec());
                    if (dist < shortestDist) { bestDir = checkdir; shortestDist = dist; }
                }
            }
            if (bestDir != null)
            {
                return CreatePortal(portal, roundPos, (bestDir ?? Dir.up));
            }

        }

        else
        {
            foreach (Dir checkdir in dirs)
            {
                if (ExtensionMethods.TileAtPos(roundPos + checkdir.ToVec()))
                {
                    float dist = Vector2.Distance(pos, roundPos + checkdir.ToVec());
                    if (dist < shortestDist) { bestDir = checkdir; shortestDist = dist; }
                }
            }
            if (bestDir != null)
            {
                return CreatePortal(portal, roundPos+(bestDir??Dir.up).ToVec(), (bestDir ?? Dir.up).Flip());
            }
        }
        return null;
    }

    private Portal CreatePortal(Portal portal, Vector3 pos, Dir dir)
    {
        //holds all colliders that are involved in the blackspot check
        List<Collider2D> tempList = new List<Collider2D>();
        tempList.AddRange(Physics2D.OverlapPointAll(pos));

        //if portal is created over black spot
        /*  bool groundOkay = false;
          foreach (Collider2D x in tempList)
          {
              if (x.tag == "Ground")
              {
                  //cancel portal
                  groundOkay = true;
              }
              if(x.tag == "BlackSpot")
              {
                  return null;
              }
          }
          if (!groundOkay) { return null; }*/
        if (ExtensionMethods.TileAtPos(pos)) ;

        //otherwise, portal is created
        return Instantiate(portal, pos, Quaternion.Euler(0, 0, dir.ToAngle()));
    }
}

public enum Dir { up, down, left, right };
