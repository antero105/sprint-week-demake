﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

public class BaseBehavior : StateMachineBehaviour
{
    protected enum DIRECTION
    {
        LEFT,
        RIGHT
    }

    protected MinionController _minionController;
    protected GameObject _minionGameObject;
    protected Animator _minionAnimator;


    protected DIRECTION CURRENT_DIRECTION;

    protected int numTries = 3;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator brain, AnimatorStateInfo stateInfo, int layerIndex)
    {

        _minionController = brain.gameObject.transform.parent.gameObject.GetComponent<MinionController>();
        _minionGameObject = brain.gameObject.transform.parent.gameObject;
        _minionAnimator = brain.gameObject.transform.parent.gameObject.GetComponent<Animator>();
        if (_minionController.CollisionEvent == null)
        {
            _minionController.CollisionEvent = new UnityEvent();
        }
        if (_minionController.target != null)
        {
            CURRENT_DIRECTION = _minionGameObject.transform.position.x > _minionController.target.transform.position.x ? DIRECTION.LEFT : DIRECTION.RIGHT;
        }
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    //override public void OnStateUpdate(Animator brain, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //}

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator brain, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator brain, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
