﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalkingStateBehavior : BaseBehavior
{

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator brain, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(brain, stateInfo, layerIndex);

        _minionController.CollisionEvent.AddListener(OnCollision);
      
    }


    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator brain, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (_minionGameObject.transform.position != _minionController.target.transform.position)
        {
            Vector2 movement = Vector2.zero;
            switch(CURRENT_DIRECTION)
            {
                case DIRECTION.RIGHT:
                    _minionGameObject.transform.position -= Vector3.right * _minionController.speed * 2 * Time.deltaTime;
                    _minionGameObject.transform.localScale = new Vector3(-1, 1, 1);
                    break;
                case DIRECTION.LEFT:
                    _minionGameObject.transform.position += Vector3.right * _minionController.speed * 2 * Time.deltaTime;
                    _minionGameObject.transform.localScale = new Vector3(1, 1, 1);
                    break;
            }
        }

    }

    private void OnCollision()
    {
        CURRENT_DIRECTION = CURRENT_DIRECTION == DIRECTION.LEFT ? DIRECTION.RIGHT : DIRECTION.LEFT;
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
