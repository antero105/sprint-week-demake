﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Simple "light" that turns green when activated from input

public class DebugLight : MonoBehaviour, IInput
{
    [SerializeField] InputComponent _input;
    SpriteRenderer sr;

    public InputComponent input { get { return _input; } set { value = _input; } }

    private void Start()
    {
        sr = GetComponent<SpriteRenderer>();
    }
    void Update()
    {
        sr.color = input.on ? new Color(0, 255, 0) : new Color(255, 0, 0);
    }
}
