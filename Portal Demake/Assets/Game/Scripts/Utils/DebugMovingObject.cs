﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Constantly moving object that can interact with portals;

public class DebugMovingObject : MonoBehaviour
{

    [SerializeField] Vector2 direction;
    [SerializeField] float speed;
    


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 movement = direction.normalized*Time.deltaTime*speed;
        transform.Translate(movement);
        
    }

    private void LateUpdate()
    {
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        direction = Vector3.Reflect(direction, collision.contacts[0].normal);
    }

  

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.GetComponent<Portal>()!=null) {
            if (collision.GetComponent<Portal>().Teleport(gameObject, out float rotate)) {
                direction.Rotate(rotate);
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.GetComponent<Portal>() != null)
        {
            collision.GetComponent<Portal>().teleportedObjects.Remove(gameObject);
        }
    }
}
