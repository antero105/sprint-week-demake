﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// 
/// A type of button that is manually toggled by the player with a tap.
/// 

public class TapButton : MonoBehaviour
{
    [SerializeField] OutputComponent output;
    SpriteRenderer sr;
    [SerializeField] bool defaultsOn;
    bool on;

    private void Start()
    {
        sr = GetComponent<SpriteRenderer>();
       output.on = defaultsOn;
    }
    private void OnMouseDown()
    {
        on = !on;

        sr.color = on ? new Color(0, 255, 0) : new Color(255, 0, 0);
        output.on = on;

    }
    void Update()
    {
    }
    
}
