﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuScript : MonoBehaviour
{
    //goes to first level when a click / tap is detected near the centre screen
    void OnMouseDown()
    {
        SceneManager.LoadScene("LevelOne");
    }
}
