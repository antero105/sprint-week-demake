﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SpawnManager : Singleton<SpawnManager>
{
    public GameObject spawnLocation;
    public GameObject minionPrefab;
    public float spawnRate = 5.0f;
    public int spawLimit = 10;

    [HideInInspector]
    public bool isActive = false;
    // Start is called before the first frame update
    void Start()
    {
        if (SceneManager.GetActiveScene().name == "LevelTwo")
        {
            ActivateSpaw();
        }
    }

   

    private void Spawn()
    {
        if (spawLimit > 0)
        {
            Instantiate(minionPrefab, spawnLocation.transform.position, Quaternion.identity);
            spawLimit -= 1;
        }
    }
    // Update is called once per frame
    void Update()
    {
       

    }

    public void ActivateSpaw()
    {
        InvokeRepeating("Spawn", spawnRate, spawnRate);
    }
}
