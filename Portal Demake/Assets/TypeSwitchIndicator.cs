﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TypeSwitchIndicator : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Image>().color = PortalManager.Instance.portalType ? new Color(1, 0.25f, 0.25f) : new Color(0.25f, 0.25f, 1);
    }

    public void Toggle() {
        if (PortalManager.Instance == null)
        {

        }
        PortalManager.Instance.portalType = !PortalManager.Instance.portalType;
        GetComponent<Image>().color = PortalManager.Instance.portalType ? new Color(1, 0.25f, 0.25f) : new Color(0.25f, 0.25f, 1);
    }

}
